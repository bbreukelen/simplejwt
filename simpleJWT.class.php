<?php
include_once __DIR__ . '/../jwt/vendor/autoload.php';
use \Firebase\JWT\JWT;

class simpleJWT {
    function __construct() {
        $this->externalAuth = null;
    }

    function setExternalAuth() {
        $this->externalAuth = func_get_args();
    }

    public function getBasicAuthDetails() {
        // Returns array with user and pass or null,null if not provided
        return [
            'user' => (isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : null),
            'pass' => (isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : null)
        ];
    }

    public function makeToken($data) {
        // Make JWT token and return the token
        // Call this function if the login was succesfull and provide the userId or whatever you want
        global $config;
        $issuedAt = time();
        $notBefore = $issuedAt + 0;
        $expire = $notBefore + 60 * $config['jwt']['sessiontime']; // 15 minute sessiontime

        $tokenData = [
            'iat' => $issuedAt,
            'jti' => base64_encode(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM)),
            'iss' => $config['jwt']['servername'],
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => $data
        ];

        $secretKey = base64_decode($config['jwt']['key']);
        $jwt = JWT::encode($tokenData, $secretKey, $config['jwt']['algorithm']);
        return $jwt;
    }

    public function authenticate() {
        // Authenticate with incoming authorization request
        global $config;
        $headers = apache_request_headers();
        if (isset($headers['Authorization']) && $headers['Authorization']) {
            list($jwt) = sscanf($headers['Authorization'], 'Bearer %s');
            if ($jwt) {
                try {
                    $secretKey = base64_decode($config['jwt']['key']);
                    $data = JWT::decode($jwt, $secretKey, array($config['jwt']['algorithm']));

                    // My own check(s)
                    if ($data->iss != $config['jwt']['servername']) { $this->Unauthorized('Unauthorized'); }

                    if ($this->externalAuth == null) {
                        return $data->data;
                    }

                    // Do external authentication check
                    $ret = call_user_func($this->externalAuth, $data->data);
                    if (gettype($ret) == 'boolean') {
                        if ($ret === false) { $this->Unauthorized('Invalid user account'); }
                        return $data->data;
                    } else {
                        return $ret; // Corrected data set
                    }

                } catch (Exception $e) {
                    $this->Unauthorized('Your session has expired');
                }

            } else {
                $this->Unauthorized('Authorization token required');
            }

        } else {
            $this->Unauthorized('Authorization token required!');
        }
    }

    private function Unauthorized($message) {
        header('HTTP/1.0 401 Unauthorized');
        if ($message != null) { print $message; }
        exit(0);
    }

    private function BadRequest($message) {
        header('HTTP/1.0 400 Bad Request');
        if ($message != null) { print $message; }
        exit(0);
    }
}